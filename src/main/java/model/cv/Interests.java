package model.cv;

public class Interests {
    private String interest;

    public Interests() {
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }
}
