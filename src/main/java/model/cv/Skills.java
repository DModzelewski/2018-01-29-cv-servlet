package model.cv;

import java.util.List;

public class Skills {

    private String summary;
    private List<String> details;

    public Skills() {
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<String> getDetails() {
        return details;
    }

    public void setDetails(List<String> details) {
        this.details = details;
    }

}
