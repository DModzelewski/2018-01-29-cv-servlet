package Listeners;

import DataStores.CvDataStore;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class CvDataStoreListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        if (sce.getServletContext().getAttribute("cvStore") != null) {
            throw new IllegalStateException("CV already exists.");
        }
        CvDataStore cvStore = new CvDataStore();
        cvStore.init();
        sce.getServletContext().setAttribute("cvStore", cvStore);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
