package Listeners;

import DataStores.ProjectDataStore;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ProjectDataStoreListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        if (sce.getServletContext().getAttribute("projectStore") != null) {
            throw new IllegalStateException("Projects already exist.");
        }
        ProjectDataStore projectStore = new ProjectDataStore();
        projectStore.init();
        sce.getServletContext().setAttribute("projectStore", projectStore);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
