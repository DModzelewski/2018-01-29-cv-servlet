package Servlets;

import DataStores.CvDataStore;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/interests", "/interests/*"})
public class InterestsServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = request.getPathInfo();
        if (path == null || "/".equals(path)) {
            getInterests(request, response);
        } else if (path.matches("^[\\/]{1}[0-9]+$")) {
            int id = Integer.parseInt(path.replace("/", ""));
            getInterest(request, response, id);
        }
    }


    private CvDataStore getStore() {
        return (CvDataStore) getServletContext().getAttribute("cvStore");
    }

    private void getInterest(HttpServletRequest request, HttpServletResponse response, int id)  throws ServletException, IOException {
        Jsonb jsonb = JsonbBuilder.create();
        String json = jsonb.toJson(getStore().getInterest(id));
        response.setContentType("application/json; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().println(json);
    }

    private void getInterests(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {
        Jsonb jsonb = JsonbBuilder.create();
        String json = jsonb.toJson(getStore().getInterests());
        response.setContentType("application/json; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().println(json);
    }
}
