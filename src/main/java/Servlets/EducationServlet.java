package Servlets;

import DataStores.CvDataStore;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/education", "/education/*"})
public class EducationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = request.getPathInfo();
        if (path == null || "/".equals(path)) {
            getEducations(request, response);
        } else if (path.matches("^[\\/]{1}[0-9]+$")) {
            int id = Integer.parseInt(path.replace("/", ""));
            getEducation(request, response, id);
        }
    }


    private CvDataStore getStore() {
        return (CvDataStore) getServletContext().getAttribute("cvStore");
    }

    private void getEducation(HttpServletRequest request, HttpServletResponse response, int id)  throws ServletException, IOException {
        Jsonb jsonb = JsonbBuilder.create();
        String json = jsonb.toJson(getStore().getEducation(id));
        response.setContentType("application/json; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().println(json);
    }

    private void getEducations(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {
        Jsonb jsonb = JsonbBuilder.create();
        String json = jsonb.toJson(getStore().getEducations());
        response.setContentType("application/json; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().println(json);
    }
}
