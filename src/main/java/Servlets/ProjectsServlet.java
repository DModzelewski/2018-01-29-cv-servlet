package Servlets;

import DataStores.ProjectDataStore;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/projects", "/projects/*"})
public class ProjectsServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = request.getPathInfo();
        if (path == null || "/".equals(path)) {
            getProjects(request, response);
        } else if (path.matches("^[\\/]{1}[0-9]+$")) {
            int id = Integer.parseInt(path.replace("/", ""));
            getProject(request, response, id);
        }
    }


    private ProjectDataStore getStore() {
        return (ProjectDataStore) getServletContext().getAttribute("projectStore");
    }

    private void getProject(HttpServletRequest request, HttpServletResponse response, int id)  throws ServletException, IOException {
        Jsonb jsonb = JsonbBuilder.create();
        String json = jsonb.toJson(getStore().getProject(id));
        response.setContentType("application/json; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().println(json);
    }

    private void getProjects(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {
        Jsonb jsonb = JsonbBuilder.create();
        String json = jsonb.toJson(getStore().getProjects());
        response.setContentType("application/json; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().println(json);
    }
}
