package Servlets;

import DataStores.CvDataStore;
import model.cv.Experience;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/experience", "/experience/*"})
public class ExperienceServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = request.getPathInfo();
        if (path == null || "/".equals(path)) {
            getExperiences(request, response);
        } else if (path.matches("^[\\/]{1}[0-9]+$")) {
            int id = Integer.parseInt(path.replace("/", ""));
            getExperience(request, response, id);
        }
    }


    private CvDataStore getStore() {
        return (CvDataStore) getServletContext().getAttribute("cvStore");
    }

    private void getExperience(HttpServletRequest request, HttpServletResponse response, int id)  throws ServletException, IOException {
        Jsonb jsonb = JsonbBuilder.create();
        String json = jsonb.toJson(getStore().getExperience(id));
        response.setContentType("application/json; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().println(json);
    }

    private void getExperiences(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {
        Jsonb jsonb = JsonbBuilder.create();
        String json = jsonb.toJson(getStore().getExperiences());
        response.setContentType("application/json; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().println(json);
    }

    private void createExperience(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Jsonb jsonb = JsonbBuilder.create();
        Experience exp = jsonb.fromJson(request.getReader(), Experience.class);
        getStore().addExperience(exp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path = req.getPathInfo();
        if ("/add".equals(path)) {
            createExperience(req, resp);
        }
    }
}
