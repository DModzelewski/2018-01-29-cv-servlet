package DataStores;

import model.project.Project;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ProjectDataStore {
    private List<Project> projects = new ArrayList<>();

    public synchronized void init(){
        Project project1 = new Project();
        project1.setName("Initial project");
        project1.setTechniques(Arrays.asList( "HTML","JavaScript","CSS"));
        project1.setSummary("This is the first summary");
        project1.setParagraphs(Arrays.asList(   "Fusce sit amet rutrum nibh. Fusce aliquam blandit ex, eu viverra orci tempus nec. Curabitur id hendrerit turpis. Integer aliquet ligula vel tempor rhoncus. Quisque condimentum, felis non vulputate scelerisque, purus mauris commodo massa, vestibulum accumsan elit velit ac ante. Pellentesque in lacus aliquet, varius lectus tincidunt, porttitor arcu. Donec at elit vitae nisi mattis semper. Aliquam egestas nisl ac orci dapibus, vitae faucibus lacus porttitor. Cras iaculis facilisis libero in elementum. Vestibulum vel felis placerat, ultricies ligula sed, aliquet tortor.",
                "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum sit amet accumsan dolor. Donec eleifend ullamcorper ullamcorper. Nam efficitur leo eu lectus vehicula rutrum. Proin suscipit vitae tellus sed tempus. Sed nisi magna, ultrices nec pharetra vel, consequat in nulla. Suspendisse vulputate eu magna at consequat. Mauris laoreet quis arcu aliquam gravida. Donec vitae tincidunt risus, rutrum accumsan est."));

        Project project2 = new Project();
        project2.setName("Second project");
        project2.setTechniques(Arrays.asList("JAVA SE","JAVA EE"));
        project2.setSummary("This is the SECOND summary");
        project2.setParagraphs(Arrays.asList("first parag", "2nd parag"));

        Project project3 = new Project();
        project3.setName("Last project");
        project3.setTechniques(Arrays.asList("Prayers","Tears","Black magic"));
        project3.setSummary("This is the very last summary so far.");
        project3.setParagraphs(Arrays.asList("Fusce sit amet rutrum nibh. Fusce aliquam blandit ex, eu viverra orci tempus nec. C"));

        projects.addAll(Arrays.asList(project1,project2,project3));
    }

    public List<Project> getProjects() {
        return Collections.unmodifiableList(projects);
    }
    public Project getProject(int id) {
        return projects.get(id);
    }
}
