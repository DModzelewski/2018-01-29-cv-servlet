package DataStores;

import model.cv.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CvDataStore {

    private PersonalData pd = new PersonalData();
    private List<Education> educations = new ArrayList<>();
    private List<Experience> experiences = new ArrayList<>();
    private List<Skills> skills = new ArrayList<>();
    private List<Interests> interests = new ArrayList<>();

    public synchronized void init(){

        pd.setName("Damian");
        pd.setSurname("Modzelewski");
        pd.setGender("Male");
        pd.setAddress("Sztormowa 7 street, Gdańsk Poland");

        Education ed1 = new Education();
        ed1.setProfile("Biological-chemical");
        ed1.setSchool("V LO High school in Gdańsk");
        ed1.setYears("2010 - 2013");

        Education ed2 = new Education();
        ed2.setProfile("Chemistry (WMD)");
        ed2.setSchool("Military Academy of Technology in Warsaw");
        ed2.setYears("2013 - 2016");

        educations.addAll(Arrays.asList(ed1,ed2));

        Experience exp1 = new Experience();
        exp1.setCompany("Army");
        exp1.setYears(Arrays.asList(2013, 2014, 2015, 2016));
        exp1.setPositions(Arrays.asList("Sergeant", "Squad leader"));

        Experience exp2 = new Experience();
        exp2.setCompany("Argo Card");
        exp2.setYears(Arrays.asList(2017, 2018));
        exp2.setPositions(Arrays.asList("Data digitalizer"));

        experiences.addAll(Arrays.asList(exp1,exp2));

        Skills sk1 = new Skills();
        sk1.setSummary("Chemistry knowledge");
        sk1.setDetails(null);

        Skills sk2 = new Skills();
        sk2.setSummary("Driving licence");
        sk2.setDetails(null);

        Skills sk3 = new Skills();
        sk3.setSummary("Programming knowledge");
        sk3.setDetails(Arrays.asList("Java",
                "C#",
                "HTML",
                "CSS",
                "JavaScript",
                "Python",
                "MySQL",
                "NoSQL"));

        Skills sk4 = new Skills();
        sk4.setSummary("Krav Maga level P1");
        sk4.setDetails(null);

        Skills sk5 = new Skills();
        sk5.setSummary("Software management");
        sk5.setDetails(Arrays.asList("Unity",
                "Adobe Photoshop",
                "Adobe After Effects",
                "Visual Studio",
                "IntelliJ",
                "Eclipse",
                "RPGMaker"));

        Skills sk6 = new Skills();
        sk6.setSummary("English level B2");
        sk6.setDetails(null);

        skills.addAll(Arrays.asList(sk1,sk2,sk3,sk4,sk5,sk6));

        Interests i1 = new Interests();
        i1.setInterest("Animals");
        Interests i2 = new Interests();
        i2.setInterest("Video games");
        Interests i3 = new Interests();
        i3.setInterest("Memes");
        interests.addAll(Arrays.asList(i1,i2,i3));
    }

    public PersonalData getPersonalData() {
        return pd;
    }

    public List<Education> getEducations() {
        return Collections.unmodifiableList(educations);
    }
    public Education getEducation(int id) {
        return educations.get(id);
    }

    public List<Experience> getExperiences() {
        return Collections.unmodifiableList(experiences);
    }
    public Experience getExperience(int id) {
        return experiences.get(id);
    }

    public List<Skills> getSkills() {
        return Collections.unmodifiableList(skills);
    }
    public Skills getSkill(int id) {
        return skills.get(id);
    }

    public List<Interests> getInterests() {
        return Collections.unmodifiableList(interests);
    }
    public Interests getInterest(int id) {
        return interests.get(id);
    }

    public synchronized void addExperience(Experience exp){
        experiences.add(exp);
    }
}
